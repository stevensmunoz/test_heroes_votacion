var btnOk = document.getElementById('btnOk');
    btnOk.addEventListener("click", function(){ addVote(this.value) } , false);

var btnOut = document.getElementById('btnOut');
    btnOut.addEventListener("click", function(){ addVote(this.value) } , false);

var btnBackLike = document.getElementById('btnBackLike');
    btnBackLike.addEventListener("click", function(){ showVote(this.value) } , false);

var btnBackDislike = document.getElementById('btnBackDislike');
    btnBackDislike.addEventListener("click", function(){ showVote(this.value) } , false);
    

function setLocalstorage() {
    let data = {
        id: 1,
        nombre: "Ironman",
        descripcion: "Anthony Edward Stark conocido como Tony Stark, es un multimillonario magnate empresarial estadounidense, playboy e ingenioso científico, quien sufrió una grave lesión en el pecho durante un secuestro en el Medio Oriente. ... Él usa el traje y las versiones sucesivas para proteger al mundo como Iron Man",
        link: "https://es.wikipedia.org/wiki/Iron_Man",
        vote: {
            vote_ok: 60,
            vote_out: 40,
            percentage_ok: 60,
            percentage_out: 40
        }
    }

    localStorage.setItem("data", JSON.stringify(data));
    getLocalstorage();
}

function getLocalstorage() {

    if(localStorage.getItem("data")){
        let data = JSON.parse(localStorage.getItem("data"));        
        this.loadPage(data);

    }else{
        this.setLocalstorage();
    }
}

function loadPage(data) {
    document.getElementById('nameHero').innerHTML = '<strong>'+data.nombre+'?</strong>';
    document.getElementById('descHero').innerHTML = data.descripcion;
    document.getElementById('linkHero').setAttribute('href', data.link);
    
    this.calculaPercentage(data.vote.vote_ok, data.vote.vote_out);
}

function calculaPercentage(ok,out) {
    
    let pOk = ((parseInt(ok) / (parseInt(ok) + parseInt(out)))*100).toFixed(1);
    let pOut = (100 - parseInt(pOk)).toFixed(1);
    
    // text
    document.getElementById('valuePercentageOk').innerHTML = ' '+pOk+'%';
    document.getElementById('valuePercentageOut').innerHTML = pOut+'% ';
    // aria-valueno
    document.getElementById('progressOk').setAttribute('aria-valuenow', pOk);
    document.getElementById('progressOut').setAttribute('aria-valuenow', pOut);
    // width
    document.getElementById('progressOk').style.width = pOk+'%';
    document.getElementById('progressOut').style.width = pOut+'%';   
}


function addVote(vote) {

    console.log(vote);
    
    let data = JSON.parse(localStorage.getItem("data"));

    let like = data.vote.vote_ok;
    let dislike = data.vote.vote_out;

    if(vote === 'like'){
        like++;
    }else{
        dislike++;
    }

    console.log('like: '+like);
    console.log('dislike: '+dislike);

    let newData = {
        id: 1,
        nombre: "Ironman",
        descripcion: "Anthony Edward Stark conocido como Tony Stark, es un multimillonario magnate empresarial estadounidense, playboy e ingenioso científico, quien sufrió una grave lesión en el pecho durante un secuestro en el Medio Oriente. ... Él usa el traje y las versiones sucesivas para proteger al mundo como Iron Man",
        link: "https://es.wikipedia.org/wiki/Iron_Man",
        vote: {
            vote_ok: like,
            vote_out: dislike,
            percentage_ok: 60,
            percentage_out: 40
        }
    }

    localStorage.setItem("data", JSON.stringify(newData));

    this.showVote(vote);
    this.calculaPercentage(like,dislike);

}

function showVote(vote) {
    let viewVote = document.getElementById('viewVote');
    let viewLike = document.getElementById('viewLike');
    let viewDislike = document.getElementById('viewDislike');
    
    switch (vote) {
        case 'like':
            viewVote.style.display = "none";
            viewLike.style.display = "block";
            
            break;
        case 'dislike':
            viewVote.style.display = "none";
            viewDislike.style.display = "block";
            
            break;
        case 'backLike':
            viewVote.style.display = "block";
            viewLike.style.display = "none";
            
            break;
        case 'backDislike':
            viewVote.style.display = "block";
            viewDislike.style.display = "none";
            
            break;
    
        default:
            break;
    }
}

getLocalstorage();